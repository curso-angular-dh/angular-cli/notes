import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotesFavoriteComponent } from './notes-favorite.component';

describe('NotesFavoriteComponent', () => {
  let component: NotesFavoriteComponent;
  let fixture: ComponentFixture<NotesFavoriteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotesFavoriteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotesFavoriteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
