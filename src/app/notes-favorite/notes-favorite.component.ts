import {Component, OnInit} from '@angular/core';
import {MainService} from '../main/main.service';

@Component({
  selector: 'app-notes-favorite',
  templateUrl: './notes-favorite.component.html',
  styleUrls: ['./notes-favorite.component.scss']
})
export class NotesFavoriteComponent implements OnInit {

  public list: { title: string, favorite: boolean }[];

  constructor(private _mainService: MainService) {
    this.list = [];
  }

  ngOnInit() {
    this.list = this._mainService.favorites;
  }

}
