import {Component, OnInit} from '@angular/core';
import {MainService} from '../main/main.service';

@Component({
  selector: 'app-notes-list',
  templateUrl: './notes-list.component.html',
  styleUrls: ['./notes-list.component.scss']
})
export class NotesListComponent implements OnInit {

  public list: { title: string, favorite: boolean }[];

  constructor(private _mainService: MainService) {
    this.list = [];
  }

  ngOnInit() {
    this.list = this._mainService.notes;
  }

}
