import {Routes} from '@angular/router';
import {NotesListComponent} from './notes-list/notes-list.component';
import {NotesFavoriteComponent} from './notes-favorite/notes-favorite.component';

export const APP_ROUTES_CONFIG: Routes = [
  {
    path: '',
    component: NotesListComponent
  },
  {
    path: '',
    component: NotesFavoriteComponent,
    outlet: 'favorites'
  }
];
