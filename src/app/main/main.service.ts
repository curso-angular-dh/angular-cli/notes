import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MainService {

  public notes: { title: string, favorite: boolean }[];
  public favorites: { title: string, favorite: boolean }[];

  constructor() {
    this.notes = [];
    this.favorites = [];
  }

  public addNote(note: { title: string, favorite: boolean }): void {
    this.notes.push(note);

    if (note.favorite) {
      this.favorites.push(note);
    }
  }
}
