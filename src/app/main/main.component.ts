import {Component, OnInit} from '@angular/core';
import {MainService} from './main.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  public text: string;
  public isFavorite: boolean;

  constructor(private _mainService: MainService) {
    this.isFavorite = false;
  }

  ngOnInit() {
  }

  public save(event: any): void {
    event.preventDefault();
    
    this._mainService.addNote({title: this.text, favorite: this.isFavorite});
    console.log(this._mainService.notes);
  }

}
